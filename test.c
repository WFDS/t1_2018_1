#include "roteador.h"
#include "simplegrade.h"
#include <assert.h>


uint32_t iptoint(int a, int b, int c, int d){
	return (uint32_t) (a<<24)+(b<<16)+(c<<8)+d;
}



//Teste simples onde temos 4 rotas, 1 filtro e 4 pacotes
//É encaminhado 1 pacote para cada enlace mas um deles deve ser filtrado
void teste_1(){
	entrada rotas[4] = { {iptoint(5,4,0,0), 16, 1},
						 {iptoint (99,0,0,0), 8, 2},
						 {iptoint(123,0,0,0), 24, 3},
						 {iptoint(200,0,1,0), 24, 4} };


	entrada filtros[1] = { {iptoint (99,0,0,0), 8, 0} };

	uint32_t pacs[4] = { iptoint(5,4,0,1), iptoint(99,0,0,1), iptoint(123,0,0,1), iptoint(200,0,1,1) };
	uint32_t * result;

	DESCRIBE("Teste 1");
	WHEN("Tenho quatro rotas disjuntas");
	IF("Envio um pacote para cada rota");

	result = roteamento(rotas, 4, pacs, 4, filtros, 1, 4);
	
	//for(int i=0; i <= 4; i++){
	//		printf("result[%d]: %d\n", i, result[i]);
	//		}

	assert(result);
	THEN("Devo ter descartado nenhum pacote");
	isEqual(result[5], 0, 1); /* nenhum pacote descartado */

	THEN("Devo ter filtrado um pacote");
	isEqual(result[0], 1, 1); /* nenhum pacote filtrado */

	THEN("Devo ter um pacote em cada enlace, menos no enlace 2");
	for(int i=1; i<=4; i++){
		if(i == 2)
			isEqual(result[i], 0, 1);
		else
			isEqual(result[i], 1, 1);
	}
	free(result);
}


//Teste com 2 Rotas e um Filtro
//O Filtro tem mascara 24 então acaba filtrando 2 pacotes dos 4 enacminhados
//sobrando apenas 2 pacotes que passam pela rota de mascara 16
void teste_2(){
	entrada rotas[2] = { {iptoint(5,4,1,0), 24, 1},
						 {iptoint(5,4,0,0), 16, 2} };

	entrada filtros[1] = { {iptoint (5,4,1,0), 24, 0} };

	uint32_t pacs[4] = { iptoint(5,4,0,1), iptoint(5,4,1,1), iptoint(5,4,1,0), iptoint(5,4,0,0)  };
	uint32_t * result;

	DESCRIBE("Teste 2");
	WHEN("Duas rotas com prefixos iguais até certo ponto");
	IF("Um pacote é enviado para cada rota");
	THEN("O resultado de conter um pacote em cada enlace");

	result = roteamento(rotas, 2, pacs, 4, filtros, 1, 2);
	assert(result);

	THEN("Devo ter descartado nenhum pacote");
	isEqual(result[3], 0, 1); /* nenhum pacote descartado */

	THEN("Devo ter filtrado dois pacote");
	isEqual(result[0], 2, 1); /* nenhum pacote filtrado */

	THEN("Devo ter 2 pacotes no enlace2, e nenhum no 1");
	for(int i=1; i<=2; i++){
		if(i == 1)
			isEqual(result[i], 0, 1);
		else
			isEqual(result[i], 2, 1);
	}
	free(result);
}

//Teste Tabela grande com 4 tipos de mascaras
//Cria 256 rotas, 64 filtros e 1280 pacotes
//Nem todos os filtros são usados
void teste_3(){
	entrada rotas[256];
	entrada filtros[64];
	uint32_t pacotes[1280];
	uint32_t * result;

	for (int i=0; i<64; i++){  //Cria uma lista de 64 filtros
		filtros[i].endereco = iptoint (12,1+i,0,0);
		filtros[i].mascara = 8;
		filtros[i].enlace = 0;
		}

	for (int i=0; i<64; i++){  //Preenchendo os 256 espaços de rotas
		for(int j=0; j<4; j++){
			/* rotas com overlap de prefixo */
			rotas[i*4+j].endereco = iptoint(10+i,1<<j,0,0);
		
			if(j == 0){
				rotas[i*4+j].mascara = 8;
				rotas[i*4+j].enlace = j+1;
				}
			if(j == 1){
				rotas[i*4+j].mascara = 16;
				rotas[i*4+j].enlace = j+1;
				}
			if(j == 2){
				rotas[i*4+j].mascara = 24;
				rotas[i*4+j].enlace = j+1;
				}
			if(j == 3){
				rotas[i*4+j].mascara = 26;
				rotas[i*4+j].enlace = j+1;
				}

			pacotes[(i*4+j)*4] = iptoint(10+i,1<<j,0,1);
			pacotes[(i*4+j)*4+1] = iptoint(10+i,1<<j,0,2);
			pacotes[(i*4+j)*4+2] = iptoint(10+i,1<<j,0,3);
			pacotes[(i*4+j)*4+3] = iptoint(10+i,1<<j,0,4);

		}
	}


	/* pacotes para serem descartados */
	for  (int i=1024; i<1280; i++)
		pacotes[i] = iptoint(99,93,11,1);


	DESCRIBE("Teste 3");
	WHEN("Teste Tabela grande com 4 tipos de mascaras");
	IF("Envio um número igual de pacotes para cada rota");
	THEN("Devo ter o mesmo número de pacotes em cada enlace");

	result = roteamento(rotas, 256, pacotes, 1280, filtros, 1, 5);
	assert(result);

	THEN("Numero de pacotes Descartados");
	isEqual(result[6], 256, 1); // descartados 

	THEN("Numero de pacotes Filtrados");
	isEqual(result[0], 16, 1); // filtrados 

	for(int i=1; i<=4; i++){
		THEN("Numero de pacotes encaminhados para o Enlace");
		printf("%s      then:%s Numero de pacotes encaminhados para o Enlace %d\n",KYEL, KNRM, i);
		isEqual(result[i], 252, 1);
	}
	free(result); 
}



int main(){


	DESCRIBE("Testes do simulador de roteador CIDR");


	teste_1();
	teste_2();
	teste_3();

	GRADEME();

	if (grade==maxgrade)
		return 0;
	else return grade;

}