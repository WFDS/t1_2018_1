#include "roteador.h"
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

//Autor: William Ferreira dos Santos

#define TAM_FILA 50        //Tamanho da fila circular (buffer)


pthread_mutex_t mutex;

uint32_t *enlace_contador;

int cont_vetorPrimario; //Contador que conta até o tamanho do vetor sendo lido, neste caso o tamanho do vetor de pacotes, definido por Num_pacotes


void qsDecrescente (entrada * rotas, int p, int r){
	int j, k, alternador;
	entrada aux;
	if (p < r){
		j = p, k = r;
		alternador = -1;
		while (j < k) { 
			if(rotas[j].mascara < rotas[k].mascara){ 
				
				aux = rotas[j];
				rotas[j] = rotas[k];
				rotas[k] = aux;

				alternador = -alternador; 
				}
			if(alternador == -1)
				--k;
			else
				++j;
			}
		qsDecrescente(rotas, p, j-1);
		qsDecrescente(rotas, j+1, r);
		}
	}



struct arg_threads {       //struct usada para passar dois argumentos para a função usada com pthread_create
	
	uint32_t *fila_circ;
	int fila_circ_inicio;
	int fila_circ_fim;
	int tamAtual_fila;
	

	entrada * rotas;
	int num_rotas;
	uint32_t * pacotes;
	int num_pacotes;
	int num_enlaces;

	entrada * filtros;
	int num_filtros;
};




void *enfileira(void *argumentos){ //cria nodo no fim da fila

	struct arg_threads *argumentosE = argumentos;
	
	while(cont_vetorPrimario < argumentosE->num_pacotes){
		if(argumentosE->tamAtual_fila == TAM_FILA){ //Fila Cheia
			
			continue; //volta para o inicio do while
			
			}
		pthread_mutex_lock( &mutex );
		
		
		
		if(argumentosE->tamAtual_fila >= 1){ //se a fila tiver 1 ou mais itens
			
			
			if(argumentosE->fila_circ_fim == TAM_FILA-1)
				argumentosE->fila_circ_fim = 0;
			else
				argumentosE->fila_circ_fim++;

			argumentosE->fila_circ[argumentosE->fila_circ_fim] = argumentosE->pacotes[cont_vetorPrimario];

			argumentosE->tamAtual_fila++;
			cont_vetorPrimario++;
			
			//break;
			}

		if((argumentosE->fila_circ_inicio == argumentosE->fila_circ_fim ) && argumentosE->tamAtual_fila == 0){ //se a fila estiver vazia
			argumentosE->fila_circ[argumentosE->fila_circ_fim] = argumentosE->pacotes[cont_vetorPrimario]; 
			
			argumentosE->tamAtual_fila++; 
			cont_vetorPrimario++; 
			
			}
		
		
		pthread_mutex_unlock( &mutex );
		
		} 

	return NULL;
	}

void *desenfileira(void *argumentos){ //deleta nodo no inicio da fila
	int  j, k, l, filtrou;

	struct arg_threads *argumentosD = argumentos;

	while((cont_vetorPrimario < argumentosD->num_pacotes) || !((argumentosD->fila_circ_inicio == argumentosD->fila_circ_fim ) && argumentosD->tamAtual_fila == 0)){ //printf("D_filaIni: %d  filaFim: %d  tam: %d\n", fila_circ_inicio[0], fila_circ_fim[0], tamAtual_fila[0]);

		
//---------------------------------------------------------
		if((argumentosD->fila_circ_inicio == argumentosD->fila_circ_fim ) && argumentosD->tamAtual_fila == 0){ //se a fila estiver vazia
			
			continue; //volta para o inicio do while
			
			}
		else{   //se a fila não estiver vazia
			
			pthread_mutex_lock( &mutex );
			
			if((argumentosD->fila_circ_inicio == argumentosD->fila_circ_fim ) && argumentosD->tamAtual_fila == 1){ //Se a fila tiver 1 item
				//------------------Tira da fila circular
				

				//------------------Filtro de IP
				l=0;
				filtrou = 0;
				while(l < argumentosD->num_filtros){ //Filtro de IP
					
					if(argumentosD->filtros[l].endereco >> (32-argumentosD->filtros[l].mascara) == (argumentosD->fila_circ[argumentosD->fila_circ_inicio] >> (32-argumentosD->filtros[l].mascara)) ){
						
						enlace_contador[0]++;
						filtrou++;
						
						}
					l++;
					}
				if(filtrou > 0){
					
					argumentosD->fila_circ[argumentosD->fila_circ_inicio] = 0;
					argumentosD->tamAtual_fila--;
					pthread_mutex_unlock( &mutex );
					continue;
					}

				for(j=0 ; j<argumentosD->num_rotas; j++){ 
					
					if((argumentosD->rotas[j].endereco >> (32-argumentosD->rotas[j].mascara)) == (argumentosD->fila_circ[argumentosD->fila_circ_inicio] >> (32-argumentosD->rotas[j].mascara))){  //Se o pacote encontrou uma rota
						k=1;
						while(k <= argumentosD->num_enlaces){
							if(argumentosD->rotas[j].enlace == k){
								enlace_contador[k]++;
								break;
								}
							k++;
							}
						break;
						}
					else{
						if(j == argumentosD->num_rotas-1){
							enlace_contador[argumentosD->num_enlaces + 1]++;
							}
						}
					}
				//------------------
				argumentosD->fila_circ[argumentosD->fila_circ_inicio] = 0;
				argumentosD->tamAtual_fila--;
				}
			
			
			if(argumentosD->tamAtual_fila > 1){ //se a fila tiver mais de 1 item
				//------------------Tira da fila circular
				

				//------------------Filtro de IP
				l=0;
				filtrou = 0;
				while(l < argumentosD->num_filtros){ //Filtro de IP
					
					if(argumentosD->filtros[l].endereco >> (32-argumentosD->filtros[l].mascara) == (argumentosD->fila_circ[argumentosD->fila_circ_inicio] >> (32-argumentosD->filtros[l].mascara)) ){
						
						enlace_contador[0]++;
						filtrou++;
						
						}
					l++;
					}
				if(filtrou > 0){
					

					argumentosD->fila_circ[argumentosD->fila_circ_inicio] = 0;

					if(argumentosD->fila_circ_inicio == TAM_FILA-1)  //Se o ponteiro de inicio estiver no ultimo indice do vetor, iguala inicio a zero
						argumentosD->fila_circ_inicio = 0;
					else
						argumentosD->fila_circ_inicio++;
					argumentosD->tamAtual_fila--;
					pthread_mutex_unlock( &mutex );
					continue;
					}


				for(j=0 ; j<argumentosD->num_rotas; j++){ 
					
					if((argumentosD->rotas[j].endereco >> (32-argumentosD->rotas[j].mascara)) == (argumentosD->fila_circ[argumentosD->fila_circ_inicio] >> (32-argumentosD->rotas[j].mascara))){  //Se o pacote encontrou uma rota
						k=1;
						while(k <= argumentosD->num_enlaces){
							if(argumentosD->rotas[j].enlace == k){
								enlace_contador[k]++;
								break;
								}
							k++;
							}
						break;
						}
					else{
						if(j == argumentosD->num_rotas-1){
							enlace_contador[argumentosD->num_enlaces + 1]++;
							}
						}
					}
				//------------------
				argumentosD->fila_circ[argumentosD->fila_circ_inicio] = 0;

				if(argumentosD->fila_circ_inicio == TAM_FILA-1)  //Se o ponteiro de inicio estiver no ultimo indice do vetor, iguala inicio a zero
					argumentosD->fila_circ_inicio = 0;
				else
					argumentosD->fila_circ_inicio++;
				argumentosD->tamAtual_fila--;
				}
			pthread_mutex_unlock( &mutex );
			}
		
		
//---------------------------------------------------------
		

		}

	return NULL;
	}



uint32_t * roteamento(entrada * rotas, int num_rotas, uint32_t * pacotes, int num_pacotes, entrada * filtros, int num_filtros, int num_enlaces){
	int i, j;
	
	
	enlace_contador = malloc(sizeof(uint32_t) * (num_enlaces+2));


	//Organizar a tabela de roteamento a partir da mascara de forma decrescente
	qsDecrescente(rotas, 0, num_rotas-1);
	

	//------------------------------------------------------------------
	int  numthreads;

	numthreads = 5;
	pthread_t thread_produtor, thread_numero[numthreads - 1];

	struct arg_threads args;

	
	mutex = (pthread_mutex_t) PTHREAD_MUTEX_INITIALIZER;


	args.fila_circ = malloc(sizeof(uint32_t) * (TAM_FILA));

	args.fila_circ_inicio = 0;
	args.fila_circ_fim = 0;
	args.tamAtual_fila = 0;

	

	args.rotas = rotas;
	args.num_rotas = num_rotas;
	args.pacotes = pacotes;
	args.num_pacotes = num_pacotes;
	args.num_enlaces = num_enlaces;

	args.filtros = filtros;
	args.num_filtros= num_filtros, 

	cont_vetorPrimario = 0;  //Variavel global

	//------------------------------------------------------------------

	for(i=0; i <= num_enlaces+1; i++){ //Laço que zera os indices de enlace_contador
			enlace_contador[i] = 0;
			}

	//consumir pacotes aqui:
	pthread_create( &thread_produtor, NULL, enfileira, (void*)&args);  //criando a thread de leitura do arquivo (produtora)
	//printf("ENTRE THREADS\n");
	for(i=0; i < (numthreads - 1) ; i++){
		//printf("2 ENTRE THREADS\n");
		pthread_create( &thread_numero[i], NULL, desenfileira, (void *)&args);  //criando as threads consumidoras(numero passado na chamada desta funcao ocorrenciasT)
		}



	pthread_join( thread_produtor, NULL);  //thread produtora espera as outras
	for(j=0; j < (numthreads - 1); j++){
		pthread_join( thread_numero[j], NULL);  //cada thread consumidora que terminar para e espera até a última acabar.
		}

	free(args.fila_circ);

	args.fila_circ = NULL;



	return enlace_contador;
	}