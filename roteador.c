#include "roteador.h"
#include <stdio.h>
#include <stdlib.h>

//Autor: William Ferreira dos Santos

void qsDecrescente (entrada * rotas, int p, int r){
	int j, k, alternador;
	entrada aux;
	if (p < r){
		j = p, k = r;
		alternador = -1;
		while (j < k) { //printf("0: %d %d \n %d | %d \n", j, k, rotas[j].mascara, rotas[k].mascara);
			if(rotas[j].mascara < rotas[k].mascara){ //(atoi(rotas[j].mascara) < atoi(rotas[k].mascara)){
				//printf("1: %d | %d\n",rotas[j].mascara, rotas[k].mascara);
				aux = rotas[j];
				rotas[j] = rotas[k];
				rotas[k] = aux;

				alternador = -alternador; //printf("2: %d | %d | %d\n",rotas[j].mascara, rotas[k].mascara, aux.mascara);
				}
			if(alternador == -1)
				--k;
			else
				++j;
			}
		qsDecrescente(rotas, p, j-1);
		qsDecrescente(rotas, j+1, r);
		}
	}

uint32_t * roteamento(entrada * rotas, int num_rotas, uint32_t * pacotes, int num_pacotes, entrada * filtros, int num_filtros, int num_enlaces){
	int i, j, k, l, filtrou;
	//uint32_t enlace_contador[num_enlaces+1];
	uint32_t *enlace_contador = malloc(sizeof(uint32_t) * (num_enlaces+2));


	//Organizar a tabela de roteamento a partir da mascara de forma decrescente
	qsDecrescente(rotas, 0, num_rotas-1);
	


	for(i=0; i <= num_enlaces +1 ; i++){ //Laço que zera os indices de enlace_contador
			enlace_contador[i] = 0;
			}



	for(i=0 ; i<num_pacotes; i++){  //printf("---Novo Pacote---\n");

		l=0;
		filtrou = 0;
		while(l < num_filtros){ //Filtro de IP
			
			if(filtros[l].endereco >> (32-filtros[l].mascara) == (pacotes[i] >> (32-filtros[l].mascara)) ){
				//printf("---Filtro = Pacote---\n");
				enlace_contador[0]++;
				filtrou++;
				
				}
			l++;
			}
		if(filtrou > 0){
			
			continue;
			}

		// Laço que passa cada rota por pacote
		for(j=0 ; j<num_rotas; j++){ 
			if((rotas[j].endereco >> (32-rotas[j].mascara)) == (pacotes[i] >> (32-rotas[j].mascara))){  //Se o pacote encontrou uma rota
				k=1;
				while(k <= num_enlaces){
					if(rotas[j].enlace == k){
						enlace_contador[k]++;
						break;
						}
					k++;
					}

				break;
				}
			else{ //Descartados
				if(j == num_rotas-1)
					enlace_contador[num_enlaces + 1]++;
				}
			}
		}

	return enlace_contador;
	}